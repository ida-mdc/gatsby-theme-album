import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout"
import LinkList from "../components/link-list"

export const query = graphql`
  query($group: String!, $name: String!) {
     versionFields: allSqliteSolution(filter:{group:{eq:$group}, name:{eq:$name}}) {
        group(field:version) {
            fieldValue
        }
    }
}
`
const NameTemplate = ({ data: { versionFields }, pageContext }) => {
    const items = []
    versionFields.group.forEach(name => {
        items.push(name.fieldValue)
    })
    return (
      <Layout site={pageContext.site}>
            <h1>Versions of <Link to={"/" + pageContext.group}>{pageContext.group}</Link>/{pageContext.name}</h1>
            <LinkList prepath={"/"+pageContext.group+"/"+pageContext.name+"/"} items={items}/>
      </Layout>
    )
}

export default NameTemplate