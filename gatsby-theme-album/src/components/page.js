import React from "react"
const Page = ({ html, frontmatter }) => (
  <>
    <h2>{frontmatter.title}</h2>
    <div dangerouslySetInnerHTML={{ __html: html }} />
  </>
)
export default Page