import React, { useState } from "react"
import { Link } from "gatsby"
import SolutionPreview from "./solution-preview"
const SolutionList = ({ solutions, tags }) => {
    const [filter, setFilter] = useState(new Set())
    return (
        <>
        <ul className="solution-list">
          {solutions.map(solution => {
            var hidden = false
            filter.forEach(tag => {
                if(!solution.tags.includes(tag)) hidden = true
            })
            if(hidden) return
            var link = "/" + solution.group + "/" + solution.name + "/" + solution.version
              return (<li key={solution.group + ":" + solution.name}>
                    <Link to={link}><SolutionPreview solution={solution}/></Link>
                </li>)
          })}
        </ul>
        </>
    )}
export default SolutionList