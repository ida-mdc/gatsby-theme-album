import React from "react"

export default function LabelBox({children}) {
  return (
    <div className="label">{children}</div>
  );
}