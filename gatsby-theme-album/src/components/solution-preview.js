import React from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

const getCover = (solution) => {
    if (solution.covers.length > 0) {
        return (<GatsbyImage className="solution-preview-image" image={getImage(solution.covers[0].img)} alt={solution.covers[0].description}/>)
    } else {
        return (<span></span>)
    }
}
const getTitle = (solution) => {
    if(solution.title) return solution.title
    else return solution.group + ":" + solution.name
}
const getDescription = (solution) => {
    if(solution.description) return (<div>{solution.description}</div>)
    else return ""
}

const SolutionPreview = ({ solution }) => {
    return (
    <div className="solution-preview">
      <div className="solution-preview-text">
          <div className="solution-title">{getTitle(solution)}</div>
          <div className="solution-description">
            {getDescription(solution)}
            {solution.solution_citations.map(citation => (<React.Fragment key={citation.text}><div className="solution-citation">{citation.text}</div></React.Fragment>))}
          </div>
       </div>
       {getCover(solution)}
    </div>
)}
export default SolutionPreview