# Gatsby theme for album workspace

### `gatsby-theme-album`

This directory is the theme package itself.

You can work on it by running
```shell
yarn workspace gatsby-theme-album develop
```

### `example`

This is an example usage of this theme. 

You can run the example with:

```shell
yarn workspace example develop
```